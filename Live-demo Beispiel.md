stages:
  - build
  - test
  - deploy

before_script:
      - echo "Ci/Cd-Pipeline gestartet"

build_stage:
  stage: build
  script:
    - echo "build stage initialisiert"
    - echo "build stage durchgeführt"

unit_test_stage:
  stage: test
  script:
    - echo "unit test stage initialisiert"
    - echo "unit test stage bestanden"

inegraion_test_stage:
  stage: test
  script:
    - echo "integration test stage initialisiert"
    - echo "integration test bestanden"

system_test_stage:
  stage: test
  script:
    - echo "system test stage initialisiert"
    - echo "system test stage bestanden"

deploy_stage:
  stage: deploy
  script:
    - echo "deploy stage initialisiert"
    - exit 1
  allow_failure: true

after_script:
      - echo "Ci/Cd-Pipeline beendet"